<?php

declare(strict_types=1);

namespace DKX\Tests;

use DKX\CallableParser\CallableParser;
use DKX\CallableParser\Callables\AnonymousFunctionCallable;
use DKX\CallableParser\Callables\FunctionCallable;
use DKX\CallableParser\Callables\MethodCallCallable;
use DKX\CallableParser\Callables\StaticMethodCallCallable;
use PHPUnit\Framework\TestCase;

final class CallableParserTest extends TestCase
{


	public function testParse_functionName(): void
	{
		/** @var \DKX\CallableParser\Callables\FunctionCallable $callable */
		$callable = CallableParser::parse('count');

		self::assertInstanceOf(FunctionCallable::class, $callable);
		self::assertEquals($callable->getName(), 'count');
	}


	public function testParse_methodCall(): void
	{
		$class = new class {
			public function test(): void {}
		};

		/** @var \DKX\CallableParser\Callables\MethodCallCallable $callable */
		$callable = CallableParser::parse([$class, 'test']);

		self::assertInstanceOf(MethodCallCallable::class, $callable);
		self::assertEquals($callable->getObject(), $class);
		self::assertEquals($callable->getClassName(), get_class($class));
		self::assertEquals($callable->getMethodName(), 'test');
	}


	public function testParse_staticMethodCall_fromArray(): void
	{
		$class = new class {
			public static function test(): void {}
		};

		$className = get_class($class);

		/** @var \DKX\CallableParser\Callables\StaticMethodCallCallable $callable */
		$callable = CallableParser::parse([$className, 'test']);

		self::assertInstanceOf(StaticMethodCallCallable::class, $callable);
		self::assertEquals($callable->getClassName(), $className);
		self::assertEquals($callable->getMethodName(), 'test');
	}


	public function testParse_staticMethodCall_fromString(): void
	{
		$class = new class {
			public static function test(): void {}
		};

		$className = get_class($class);

		/** @var \DKX\CallableParser\Callables\StaticMethodCallCallable $callable */
		$callable = CallableParser::parse($className. '::test');

		self::assertInstanceOf(StaticMethodCallCallable::class, $callable);
		self::assertEquals($callable->getClassName(), $className);
		self::assertEquals($callable->getMethodName(), 'test');
	}


	public function testParse_anonymousFunction(): void
	{
		$function = function() {};

		/** @var \DKX\CallableParser\Callables\AnonymousFunctionCallable $callable */
		$callable = CallableParser::parse($function);

		self::assertInstanceOf(AnonymousFunctionCallable::class, $callable);
		self::assertEquals($callable->getFunction(), $function);
	}

}
