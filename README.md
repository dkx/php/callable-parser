# DKX/CallableParser

Callable parser

## Installation

```bash
$ composer require dkx/callable-parser
```

## Usage

```php
<?php

namespace DKX\CallableParser\CallableParser;

$callable = getCallableSomehow();
$parsed = CallableParser::parse($callable);
```

The `parse()` method can return one of the following classes:

* `DKX\CallableParser\Callables\FunctionCallable`: passed function name
* `DKX\CallableParser\Callables\MethodCallCallable`: passed instantiated object and method name (in array)
* `DKX\CallableParser\Callables\StaticMethodCallCallable`: passed either string with `className::methodName` notation or array with class name and method name 
* `DKX\CallableParser\Callables\AnonymousFunctionCallable`: passed anonymous function  
