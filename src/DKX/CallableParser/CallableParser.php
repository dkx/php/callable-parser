<?php

declare(strict_types=1);

namespace DKX\CallableParser;

use DKX\CallableParser\Callables\AnonymousFunctionCallable;
use DKX\CallableParser\Callables\CallableInterface;
use DKX\CallableParser\Callables\FunctionCallable;
use DKX\CallableParser\Callables\MethodCallCallable;
use DKX\CallableParser\Callables\StaticMethodCallCallable;

final class CallableParser
{


	public static function parse(callable $callable): CallableInterface
	{
		if (is_string($callable)) {
			$parts = explode('::', $callable);
			$partsCount = count($parts);

			if ($partsCount === 1) {
				return new FunctionCallable($callable);
			}

			if ($partsCount === 2) {
				return new StaticMethodCallCallable($parts[0], $parts[1]);
			}
		}

		if (is_array($callable)) {
			if (is_object($callable[0]) && is_string($callable[1])) {
				return new MethodCallCallable($callable[0], $callable[1]);
			}

			if (is_string($callable[0]) && is_string($callable[1])) {
				return new StaticMethodCallCallable($callable[0], $callable[1]);
			}
		}

		return new AnonymousFunctionCallable($callable);
	}

}
