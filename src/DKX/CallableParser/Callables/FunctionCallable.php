<?php

declare(strict_types=1);

namespace DKX\CallableParser\Callables;

final class FunctionCallable implements CallableInterface
{


	/** @var string */
	private $name;


	public function __construct(string $name)
	{
		$this->name = $name;
	}


	public function getName(): string
	{
		return $this->name;
	}

}
