<?php

declare(strict_types=1);

namespace DKX\CallableParser\Callables;

final class AnonymousFunctionCallable implements CallableInterface
{


	/** @var callable */
	private $function;


	public function __construct(callable $function)
	{
		$this->function = $function;
	}


	public function getFunction(): callable
	{
		return $this->function;
	}

}
