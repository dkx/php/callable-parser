<?php

declare(strict_types=1);

namespace DKX\CallableParser\Callables;

final class StaticMethodCallCallable extends AbstractMethodCallCallable
{


	/** @var string */
	private $className;


	public function __construct(string $className, string $methodName)
	{
		parent::__construct($methodName);

		$this->className = $className;
	}


	public function getClassName(): string
	{
		return $this->className;
	}

}
