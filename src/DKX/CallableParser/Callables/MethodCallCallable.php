<?php

declare(strict_types=1);

namespace DKX\CallableParser\Callables;

final class MethodCallCallable extends AbstractMethodCallCallable
{


	/** @var object */
	private $object;


	public function __construct(object $object, string $methodName)
	{
		parent::__construct($methodName);

		$this->object = $object;
	}


	public function getClassName(): string
	{
		return get_class($this->object);
	}


	public function getObject(): object
	{
		return $this->object;
	}

}
