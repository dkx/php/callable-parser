<?php

declare(strict_types=1);

namespace DKX\CallableParser\Callables;

abstract class AbstractMethodCallCallable implements CallableInterface
{

	/** @var string */
	private $methodName;


	public function __construct(string $methodName)
	{
		$this->methodName = $methodName;
	}


	abstract public function getClassName(): string;


	public function getMethodName(): string
	{
		return $this->methodName;
	}

}
